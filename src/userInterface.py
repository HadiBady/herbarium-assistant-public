"""from in_gifts import in_gift
from in_loans import in_loan
from out_gifts import out_gift
from out_loans import out_loan
#from filehandlerv2 import FileMang"""
import sys
import filefixer
#import filehandlerv2
#print(file_manager)

### this data_type is an object which will hold the name of the date type
## as well as the map that that specific data type has, this map allows us to access
## specific fields later on much easily by using an array with indexes that reflect the 
## mapping example, id maps to 0, so when user types in "id", i use index 0
class data_type():
    def __init__(self, name, map):
        self.__name = name
        self.__map = map
    
    def get_map(self):
        return self.__map
    
    def get_name(self):
        return self.__name
    
    def set_map(self, map):
        self.__map = map
    
    def set_name(self,name):
        self.__name = name

### the basic_info class is not actually basic as in simple, rather this is the class with the least amount of
## stuff required to form an object to hold the information in it
## here it will have a data_type which has the type name as well as the map, we also have an array
## that will hold the values in as well
## we also store the type so that we know what type of data is stored
class basic_info():
    def __init__(self):
        self.__id = sys.maxsize
        self.__info = []
        self.__data_type=None
        self.__type = "unknown"

    def __init__(self, id, packet, data_type, type):
        self.__id = id
        self.__info = packet.copy()
        self.__data_type=data_type
        self.__type = type
    
    def get_attribute(self, id):
        if (id < len(info) and id >-1):
            return info[id]
        else:
            return None
    
    def set_attribute(self, id, new_info):
        if (type(id) is int):
            if (id < len(info) and id >-1):
                self.__info[id] = new_info
        elif (type(id) is string):
            if (id in self.__mappping):
                self.__info[self.__mapping[id]] = new_info
    
    
    def override_packet(self, id, packet, data_type, type):
        self.__info = packet.copy()
        self.__data_type = data_type
        self.__type = type
    
    def get_type(self):
        return self.__type
    
    def get_data_type(self):
        return self.__data_type
    
    def set_type(self, type):
        self.__type = type
    
    def get_data_type(self, data_type):
        self.__data_type = data_type
    
### this catalogue_container will contain the data that is stored in the csv files,
## it will also store the filename, what datatype is present in this csv file
class catalogue_container(object):
    def __init__(self, filename, datalist, datatype):
        self.__filename = filename
        self.__datalist = datalist
        self.__datatype = datatype

    def get_data_list(self):
        return self.__datalist
    
    def set_data_list(self, newdatalist):
        self.__datalist = newdatalist
        
    def get_filename(self):
        return self.__filename
    
    def set_filename(self, newname):
        self.__filename = newname

    def save_catalogue(self):
        file = open(filename, 'w')
        
        file.write(self.__datatype.get_name())
        
        string_to_write = ""
        for x in (self.__datatype.get_map()):
            string_to_write += "\""+ self.__datatype[x] + "\","
            
        string_to_write = string_to_write[:-1]
        
        file.write(string_to_write)
        
        string_to_write = ""
        
        for x in (self.__datalist):
            for i in self.__datalist[x]:
                string_to_write += "\""+ self.__datalist[x] + "\","
            string_to_write = string_to_write[:-1]
            file.write(string_to_write)
        
        file.close()

### file_manager will actually be the class that creates the data_types
## and will also analyze the files to ensure that the proper data_type, basic_info, and catalogue container is created
## it will also be the class that can display to use our history, which is when was the last time we opened the file as well
## as well as which files, and which opnes opened successfully and which ones did not
class file_manager(object):
    unknown_str = "?"
    counter = 0
    
    def __init__(self):

        default_data_types = open("workspace/DataRep/datatypes.txt", "r")
        
        custom_data_types = open("workspace/DataRep/customdatatypes.txt", "r")
        
        default_data_lines = default_data_types.readlines()
        
        custom_data_lines = custom_data_types.readlines()
        
        self.__typesDictionary = {}
        self.__new_types_dictionary={}
        tempDict = {}
        
        flag = False
        
        typename = ""
        
        for x in range (0, len(default_data_lines)):
            default_split = default_data_lines[x].split("\t",1)
            #print(tempDict)
            #print(default_split)
            if (len(default_split) > 1 and default_split[0] == "default" ):
                flag = True
                typename = default_split[1].rstrip()
                continue
            elif (len(default_split) > 1 and flag ):
                tempDict[int(default_split[0])] = default_split[1].rstrip()
            else:
                flag = False
                tempType = data_type(typename,tempDict.copy())
                self.__typesDictionary[typename] = tempType
                tempDict = {}
                tempname=""
                tempType=None
                print(self.__typesDictionary[typename].get_map())
        
        flag = False
        
        typename = ""
        tempDict = {}
        
        for x in range (0, len(custom_data_lines)):
            default_split = custom_data_lines[x].split("\t",1)
            if (len(default_split) > 1 and default_split[0] == "custom"):
                flag = True
                typename = default_split[1].rstrip()
                continue
            elif (len(default_split) > 1 and flag ):
                tempDict[int(default_split[0])] = default_split[1].rstrip()
            else:
                flag = False
                tempType = data_type(typename,tempDict.copy)
                self.__typesDictionary[typename] = tempType
                tempDict = {}
        
        #print(self.__typesDictionary)
        
        custom_data_types.close()
        default_data_types.close()
        #print(self.__typesDictionary)
        #print(self.__new_types_dictionary)
        
        self._catalogue = {}
        self._ID = 0


    def openFile(self, filename):
        id = 0

        #file = open (filename, 'r')
        
        file_lines = filefixer.convert_new_line(filename)#file.readlines()
        
        item_number = 0
        
        catalogue = None
        datalist = []
        
        ## this if statements makes sure that we have the valid type already created if not then we create the type before moving on
        print(file_lines[0])
        f = comma_delimiter(file_lines[0])
        print(f)
        #r = quotes_remover(f)
        #print(r)
        t = hashmapmaker(f)
        print(self.__typesDictionary)
        hm = 0
        for x in self.__typesDictionary:
            print(t)
            print(x)
            print(self.__typesDictionary[x].get_map() )
            if (hashmapcompare(self.__typesDictionary[x].get_map(),t)):
                hm = x
                break
        if (hm ==0):
            for x in self.__new_types_dictionary:
                if (hashmapcompare(self.__typesDictionary[x].get_map(),t)):
                    hm = x
                    break
        
        #not (file_lines[0] in self.__typesDictionary)
        if (hm == 0):
            while(True):
                print("The following type", file_lines[0], "has not been created before.")
                answer = input("Would you like to continue analyzing this file and add it in?\n0)Yes\n1)No!")
            
                if(answer == "0"):
                    print("Okay will add new type\"" + file_lines[0] + "\" to our custom data types")
                    # definitelly make this into a function
                    default_split = comma_delimiter(file_lines[1])
                    default_split = quotes_remover(default_split)
                    
                    item_number =len(default_split)
                    tempDict = {}
                    
                    for i in range (0, len(default_split)):
                        tempDict[i] = default_split[1]
                    
                    tempType = data_type(default_lines[0], tempDict)
                    self.__new_types_dictionary[typename] = tempType
                    break
                    
                elif(answer == "1"):
                    print("Alright, then that means we have to return as the file cannot be parsed.")
                    return "file invalid type"
                else:
                    print("Please enter in a valid response.")
                    
        else:
            item_number = len(self.__typesDictionary[hm].get_map())
        # here we only continue if the type is valid
        
        # now actually making the catalogue type and will return that back
        
        for i in range(1, len(file_lines)):
            default_split = comma_delimiter(file_lines[i])
            print(default_split)
            #default_split = quotes_remover(default_split)
            
            if(len(default_split) != item_number):
                print("Error: data items in the file are not all consistent, variable number of information")
                return "data inconsistent"
            
            type = "unknown"
            tempDataType=None
            tempInfo = None
            
            if (hm != 0):
                tempDataType = self.__typesDictionary[hm]
                type = "default"
            else:
                tempDataType = self.__new_types_dictionary[hm]
                type = "custom"

            tempinfo = basic_info(id, default_split, tempDataType, type)
            datalist.append(tempinfo)
            id+=1
        
        catalogue=catalogue_container(filename, datalist, tempDataType)
            
        return catalogue

    def history(self):
        self.__file_init = open("workspace/HerbAssistSettings/data_initialization_files.txt", "r")
        self.__lines = self.__file_init.readlines()
        i = 0
        
        while(i < len(self.__lines)):
            line_split = self.__lines[i].split("\t")
            if (line_split[0] == "previous_file_initialization" ):
                print(line_split[1], "previous file(s) have been initialized on", line_split[2],"are:")
                for x in range(i, i+int(line_split[1])):
                    print(self.__lines[x+1])
                    i = x+1
            elif (line_split[0] == "previous_broken_files"):
                print(line_split[1], "previous file(s) that were not initialized on", line_split[2],"are:")
                for x in range(i, i+int(line_split[1])):
                    print(self.__lines[x+1])
                    i = x+1
                break
            i+=1
        
        self.__file_init.close()
        
        return "success"


## this hashmapmaker makes a hashmap from each list
def hashmapmaker(list):
    tempHM = {}
    value = 0
    for x in list:
        tempHM[value] = x
        value+=1
    return tempHM

## had to make this as cmp is gone from python, this basically compares
## 2 hashmaps and sees if they are similar or not
def hashmapcompare(dict0,dict1):
    if (len(dict0) != len(dict1)):
        return False
    
    for x in dict0:
        #for y in dict1:
        if (dict0[x] != dict1[x]):
            return False
    
    return True
                

## this function will check if a the string only contains characters to make sure it is a valid acroynum
def char_check(acro):
    # create a function that searchs through a string to see  if any digits are in the string
    return acro.isalpha()


## this function will check to make sure only a number value is present in this item
def num_check(num):
    try:
        temp = num.isdigit()
    except:
        temp = False
    return temp

## this function checks to see if this field has the correct number of items in it.
def item_num_check(items, num):
    return (items.split(" ") == num)

## this function returns to you the string which was selected to be the unknown string
def unknown_string():
    return unknown_str


## a function to split a string by the comma, but only commas that have double quotes before and after the comma
## I also accidently added the "feature" of removing quotes at the end and beginning of the word, so no need for quotes_remover
def comma_delimiter(string):
    c = len(string)
    i = 1
    lastsplit = 0
    tokens = []
    while (i < c):
        if (i + 1 == c):
            tokens.append(string[lastsplit+1: c-1])
        elif ((string[i-1] == "\"") and (string[i] == ",") and (string [i+1] == "\"")):
            tokens.append(string[lastsplit+1:i-1])
            lastsplit = i+1
        i +=1
    return tokens

## a function to remove the quotes at the beginning and end of the string, as CSV files add quotes as well as commas to separate items
def quotes_remover(list):
    c = 0
    tempList = list
    while (c < len(tempList)):
        tempList[c] =tempList[c][1:len(tempList[c])-1] 
        #tempList[c].replace("\"")
        #tempList[c] = #tempList[c].lstrip("\"")
        c +=1
    return tempList

### this the main part of the program, this is the class that will have
## the functions defined so we can play around wiht our catalogue, this will
## allow us to edit the data, remove data, save our changes, list the data, and most importantly,
## search through the data to find what we desire.
class catalogue_tool(object):

    def __init__(self, catalogue_group):
        self.__status = False
        self.__catalogue_group = catalogue_group
    
    def command(self):
        #self.__status = True
        response = input("Please enter a command:")
        
        response_split = response.split()
        if (len(response_split) >0):
            if(response_split[0] == ""):
                pass
            elif(response_split[0] == "exit"):
                pass
            elif(response_split[0] == "view"):
                if(response_split[0] == "data"):
                    pass
                else:
                    pass
            elif(response_split[0] == "add"):
                if(response_split[1] == "column"):
                    pass
                elif(response_split[1] == "data"):
                    pass
                elif(response_split[0] == "sub-catalogue"):
                    pass
                else:
                    pass
            elif(response_split[0] == "save"):
                pass
            elif(response_split[0] == "select"):
                if(response_split[1] == "data"):
                    pass
                elif(response_split[1] == "sub-catalogue"):
                    pass
                elif(response_split[0] == "view"):
                    pass
                elif(response_split[0] == "add"):
                    pass
                elif(response_split[0] == "save"):
                    pass
            elif(response_split[0] == "edit"):
                if(response_split[0] == ""):
                    pass
                elif(response_split[0] == "exit"):
                    pass
                elif(response_split[0] == "view"):
                    pass
                elif(response_split[0] == "add"):
                    pass
                elif(response_split[0] == "save"):
                    pass
            elif(response_split[0] == "search"):
                if(response_split[0] == ""):
                    pass
                elif(response_split[1] == "data"):
                    pass
                elif(response_split[2] == "in"):
                    pass
                else:
                    pass
            elif(response_split[0] == "list"):
                if(response_split[0] == ""):
                    pass
                elif(response_split[0] == "details"):
                    if(response_split[0] == ""):
                        pass
                    elif(response_split[0] == "current"):
                        pass
                else:
                    pass
            elif(response_split[0] == "delete"):
                if(response_split[0] == ""):
                    pass
                elif(response_split[0] == "id"):
                    pass
                elif(response_split[0] == "column"):
                    pass
                elif(response_split[0] == "sub-catalogue"):
                    pass
                else:
                    pass
            elif(response_split[0] == "change"):
                if(response_split[0] == ""):
                    pass
                elif(response_split[0] == "column-name"):
                    pass
                elif(response_split[0] == "cub-catalogue"):
                    pass
                else:
                    pass
            else:
                print("invalid input, please try again")
        else:
            print("invalid input, please try again")
        print(response)
        return response
    
    def status(self):
        return self.__status
        
    def exitcode(self):
        return "exit"

### this is also another important class, but this class is just the user interface,
## it basically allows the user to interact with the program so that we can get
## what the user needs to do much easily rather than having them to type out 
## commands on the terminal
class userInterfaceCMD:
    __instance = None
    __all_files_initialized = False
    __partial_files_initialized = False
    __initialized_files_list = []
    __non_initialized_files = []
    __files_list = []
    __initialize = 0
    __catalogue_started = False
    __catalogue = []
    __catalogue_tool= None
    
    __file_manager = None
    
    __catalogue_changed = False
    
    @staticmethod
    def getInstance():
        if userInterfaceCMD.__instance == None:
            userInterfaceCMD
        return userInterfaceCMD.__instance
    
    
    def __init__(self):
        
        if (userInterfaceCMD.__instance !=None):
            raise Exception ("Singleton Design class")
        
        self.introduction()
        reply = self.options()
        ## the point of this is that we can have the user enter in a invalid choice, without filling up the stack with unnecessary function calls
        while(True):
            reply = self.options()
        print(reply)
    
    
    def introduction(self):
        print("Welcome to Python Herbarium Assistant\n"
        "This application is designed to help out " 
        "cateloguing the loans and gifts of different \nplants," 
        " kind of like a library book keeping system. \n"
        "Its basically a catalogue system.")
        

    def options(self):
        print("\n\nPlease select one of the following options: ")
        
        print("0) Setting up the Herbarium Assistant")
        print("1) Saving the current progress")
        print("2) Setting up the emailing system")
        print("3) Performing an operation on the catalogue")
        print("4) Exit")
        
        response = input("")
        
        if (response == "0"):
            self.import_files()
        elif(response == "1"):
            self.save_files()
        elif(response == "2"):
            self.email_setup()
        elif(response == "3"):
            self.catalogue_use()
        elif(response == "4"):
            self.exiting()
        else:
            print("Invalid input",response,"please try again.")
            return None
        return response

    def import_files(self):        
        # here we state the previous history of files being opened
        if (userInterfaceCMD.__file_manager == None):
            print("Setting up FileManager to aid in initialization of files")
            userInterfaceCMD.__file_manager = file_manager()
        
        userInterfaceCMD.__file_manager.history()
        while(True):
            print("\n\nSelect from the following options:")
            print("0)Load in the default files")
            print("1)Load in a new / different file")
            print("2)Return")
            answer = input()
            if (answer == "0"):
                file = open("workspace\HerbAssistSettings\data_files_to_load.txt", "r")
                
                lines = file.readlines()
                print(lines)
                if (len(userInterfaceCMD.__files_list) == 0):
                    userInterfaceCMD.__files_list = lines
                    
                counter = 0
                for i in range(0, len(lines)):
                    print(lines[i].rstrip('\n'))
                    result = userInterfaceCMD.__file_manager.openFile(lines[i].rstrip('\n'))
                    #print(result)
                    if (type(result) is str):
                        counter +=1
                        userInterfaceCMD.__all_files_initialized = False
                        userInterfaceCMD.__non_initialized_files.append(lines[i])
                    else:
                        #userInterfaceCMD.__files_list.append(lines[i])
                        userInterfaceCMD.__catalogue.append(result)
                        userInterfaceCMD.__initialized_files_list = []

                
                if (counter == len(lines)):
                    userInterfaceCMD.__partial_files_initialized = False
                elif (counter == 0):
                    userInterfaceCMD.__all_files_initialized = True
                else:
                    userInterfaceCMD.__partial_files_initialized = True
                self.__initialize = -1
    
                
            elif(answer =="1"):
                filename = input("Please type in the file location:")
                
                if (filename in userInterfaceCMD.__files_list):
                    print("You cannot reinitialize", filename, "after already initializing")
                    continue
                
                file = open(filename, "r")
                result = userInterfaceCMD.__file_manager.openFile(lines[i])
                
                if (not type(result) is string):
                    userInterfaceCMD.__catalogue.append(result)
                self.__initialize = -1
                    
            elif(answer == "2"):
                break
            else:
                print("Please try again.")
    
    ## for now this function simply terminates the program, but what will happen is that we have to ensure that the files are saved, the integrity is checked and the nothing is "lost"
    def exiting(self):
        if (self.__catalogue_changed == False):
            sys.exit()
        print("Cannot close software as changes to the catalogue have not been saved.")
        reply = input("Would you like to close the software without saving your catalogue?\n0)NOOOO!!!!!\n1)Yes\n")
        
        if (reply == "0"):
            print("That was a close one... don't want to lose all of your hard work.")
            return
        elif (reply == "1"):
            print("Terminating software....")
            sys.exit()

    def email_setup(self):
        print("Not implemented Yet!")

    def catalogue_use(self):
        if (self.__initialize == 0):
            print("You have not initalized any files")
            return
        
        if (self.__catalogue_tool == None or self.__initialize == -1):
            print("Starting up the catalogue")
            self.__catalogue_tool = catalogue_tool(userInterfaceCMD.__catalogue)
            self.__catalogue_started = True
            self.__initialize = 1
        #print(self.__catalogue)
        while(True):
            response = self.__catalogue_tool.command()
            if(response == self.__catalogue_tool.exitcode()):
                break
        
        self.__catalogue_changed = self.__catalogue_tool.status()
        #print("Not implemented Yet!")
    
    def save_files(self):
        print("\n\nSaving all files with their current names.")
        
        if(len(userInterfaceCMD.__catalogue) == 0):
            print("No files were initalized for us to save any work...")
            return
        
        for i in range(0, len(userInterfaceCMD.__catalogue)):
            userInterfaceCMD.__catalogue[i].save_catalogue()
            print("Catalogue",userInterfaceCMD.__catalogue[i].get_filename(),"saved successfully.")
        
        print("All files saved!")
        #print("Not implemented Yet!")

if __name__ == '__main__':
    interface = userInterfaceCMD()