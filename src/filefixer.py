def convert_new_line(filename):
    file = open(filename, "r")

    list = []

    tempStr = ""
    partialList = []

    c1 = file.read(1)
    c2 = file.read(1)
    c3 = file.read(1)

    while (True):

        if ( (c1 == '\"' and c3 == '\"') and c2 == '\n'):
            tempStr += c1
            list.append(tempStr)# instead store \\n in that spot
            tempStr = ""
            c1 = c3
            c2 = file.read(1)
            c3 = file.read(1)
        else:
            tempStr += c1
            c1 = c2
            c2 = c3
            c3 = file.read(1)

        if(not c1 or not c2 or not c2):
            break
        #print(tempStr)

    if (c1 != False):
        tempStr += c1

    if (c2 != False):
        tempStr+= c2

    if (c3 != False):
        tempStr += c3

    list.append(tempStr)

    return list


#list = convert_new_line("temp")
#print(list)

temp = "\"test\""

print(temp[1:len(temp)-1])