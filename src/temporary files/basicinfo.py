class basic_info():

	def __init__(self):
		self.__info = []
		self.__mapping = []
		self.__label = "basic_info"

	def __init__(self, packet, mapping, label):
		self.__info = packet.copy()
		self.__mapping = mapping.copy()
		self.__label = label

	def get_attribute(self, id):
		if (id < len(info) and id >-1):
			return info[id]
		else:
			return None

	def set_attribute(self, id, new_info):
		if (type(id) is int):
			if (id < len(info) and id >-1):
				self.__info[id] = new_info
		elif (type(id) is string):
			if (id in self.__mappping):
				self.__info[self.__mapping[id]] = new_info


	def override_packet(self, packet, mapping, label):
		self.__info = packet
		self.__mapping = mapping
		self.__label = label
