# Developed by Hadi Badawi
# The purpose of this file is to handle any importing of cvs files for the
# Western Herbarium Labs

from in_gifts import in_gift
from in_loans import in_loan
from out_gifts import out_gift
from out_loans import out_loan

class FileMang:
	unknown_str = "?"

	def __init__(self):

		self._Dictionary = {}
		self._catalogue = {}
		self._ID = 0


	def openFile(self, filename, type):
		try:
			contFile =open(filename, "r")


			if (type == "in_gifts"):
				self.inGifts(contFile, 0)
			elif (type == "in_loans"):
				self.inLoans(contFile, 0)
			elif (type == "out_gifts"):
				self.outGifts(contFile)	
			elif (type == "out_loans"):
				self.outLoans(contFile)
			elif (type == "in_loans_request"):
				self.inGifts(contFile, 1)
			else:
				raise Exception("The type of the document analysized is not valid")
					
		except FileNotFoundError:
			print("Error: File(s) was/are found.")
		finally:
			contFile.close()

	def inGifts(self, file, request):
		tempList = []

		line = file.readline()

		while line != "":

			line = line.rstrip()
			tempList = comma_delimiter(line) # need to replace this with something that only separates by commas if the character before and after is a ""
			tempList = quotes_remover(tempList)
			

			tempgift = in_gift(self._ID,  tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8])
			self._ID += 1

			print(tempList)

			line = file.readline()

	"""

	id, request, loan_code, loan_status, acr_of_inst, institute,
			person_of_contact, institute_address, date_requested, invoice_date,
		loan_due_date, num_specimens_received, date_returned,
		instution_loan_closed_date, specs_returned,
		sub_balance_for_multi_return, balance, under_study,
		 outstanding, spec_desc, notes, comments, paperwork_found, digitized)
	"""
	def inLoans(self,file):
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)
			temploans = in_loan(self._ID, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10], tempList[11], tempList[12], tempList[13], tempList[14], tempList[15], tempList[16], tempList[17], tempList[18], tempList[19], tempList[20], tempList[21], tempList[22])
			self._ID += 1
			
			print(tempList)

			line = file.readline()

	"""
	self, id, gift_num,  institution, contact,
		address, date_sent, date_recv, spec_num,
		spec_desc, notes, paperwork, digitized
	"""
	def outGifts(self,file):
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)

			tempgift = out_gift(self._ID, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10])
			self._ID += 1

			print(tempList)

			line = file.readline()

	"""
	id, request, loan_code, loan_status, acr_of_inst, institute,
			person_of_contact, institute_address, date_requested,sent_date,
		loan_due_date, num_specimens_received, date_returned,
		instution_loan_closed_date, specs_returned,
		sub_balance_for_multi_return, balance, under_study,
		outstanding, spec_desc, for_study_by, herb_stack_check,
		notes_comments, paperwork_found, digitized
	"""
	def outLoans(self,file):
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)

			temploans = out_loan(self._ID, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10], tempList[11], tempList[12], tempList[13], tempList[14], tempList[15], tempList[16], tempList[17], tempList[18], tempList[19], tempList[20], tempList[21], tempList[22])
			self._ID += 1

			print(tempList)

			line = file.readline()

	## this function will save the stuff to a file with the specified output name
	def saveInventoryCatalogue(self, filename):
		file = open(filename, "w")
		file.write("Name|Continent|Population|PopulationDensity\n")
		i = 0
		for c in self._catalogue:
			if len(self._catalogue) > 0:
				toWrite = str(self._catalogue[c].getName() + "|" + self._catalogue[c].getContinent() + "|" +
				str(self._catalogue[c].getPopulation()) + "|" + "%.2f" %(self._catalogue[c].getPopDensity()) + "\n")
				file.write(toWrite)
				i += 1
			file.close()
		return i

## temp comment

## this function will check if a the string only contains characters to make sure it is a valid acroynum
def char_check(acro):
		# create a function that searchs through a string to see  if any digits are in the string
	return acro.isalpha()
		
	
## this function will check to make sure only a number value is present in this item
def num_check(num):
	return num.isdigit()
		
## this function checks to see if this field has the correct number of items in it.
def item_num_check(items, num):
	return (items.split(" ") == num)
		
## this function returns to you the string which was selected to be the unknown string
def unknown_string():
	return unknown_str
		
		
	## a function to split a string by the comma, but only commas that have double quotes before and after the comma
def comma_delimiter(string):
	c = len(string)
	i = 1
	lastsplit = 0
	tokens = []
	while (i < c):
		if (i + 1 == c):
			tokens.append(string[lastsplit: c])
		elif ((string[i-1] == "\"") and (string[i] == ",") and (string [i+1] == "\"")):
			tokens.append(string[lastsplit+1:i-1])
			lastsplit = i+1
		i +=1
	return tokens

## a function to remove the quotes at the beginning and end of the string, as CSV files add quotes as well as commas to separate items
def quotes_remover(list):
	c = 0
	tempList = list
	while (c < len(tempList)):
		tempList[c].rstrip("\"")
		tempList[c].lstrip("\"")
		c +=1
	return tempList

## follow this to turn all CSV files to have double quotes around the text

if __name__ == '__main__':
	
	temp = FileMang()
	temp.openFile('in_loans_request_double_quotes.csv', 'in_loans_request')
