# Developed by Hadi Badawi
# The purpose of this file is to handle any importing of cvs files for the
# Western Herbarium Labs
"""from in_gifts import in_gift
from in_loans import in_loan
from out_gifts import out_gift
from out_loans import out_loan
"""
from os import sys, path
#sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import basicinfo

class file_manager(object):
	unknown_str = "?"

	def __init__(self, filenames):
		self.__filenames = filenames
		self.__file_count = len(self.__filenames)
		
		self.__file_init = open("../HerbAssistSettings/data_initialization_files.txt", "w+")
		
		self.__lines = self.__file_init.readlines()
		
		i =0
		
		while(True):
			line_split = self.__lines[i].split("\t")
			if (line_split[0] == "previous_file_initialization" ):
				print(line_split[1], "previous file(s) have been initialized on", line_split[2],"are:")
				for x in range(i, line_split[1]):
					print(self.__lines[x+1])
					i = x+1
			elif (line_split[0] == "previous_broken_files"):
				print(line_split[1], "previous file(s) that were not initialized on", line_split[2],"are:")
				for x in range(i, line_split[1]):
					print(self.__lines[x+1])
					i = x+1
				break
		
		self._Dictionary = {}
		self._catalogue = {}
		self._ID = 0


	def openFile(self, filename, type):
		try:
			contFile =open(filename, "r")
			if (type == "in_gifts"):
				self.inGifts(contFile)
			elif (type == "in_loans"):
				self.inLoans(contFile, 0)
			elif (type == "out_gifts"):
				self.outGifts(contFile)	
			elif (type == "out_loans"):
				self.outLoans(contFile)
			elif (type == "in_loans_request"):
				self.inLoans(contFile, 1)
			else:
				raise Exception("The type of the document analysized is not valid")
					
		except FileNotFoundError:
			print("Error: File(s) was/are found.")
		finally:
			contFile.close()

	def inGifts(self, file):
		tempList = []
		toReturn = []
		brokenList = []
		
		lines = file.readlines()
		
		
		#while line != "":
		for line in lines:
			
			line = line.rstrip()
			tempList = comma_delimiter(line) # need to replace this with something that only separates by commas if the character before and after is a ""
			tempList = quotes_remover(tempList)


			#if (not (num_check(num_spec_rec)) or not (char_check(arc)) or not(num_check(id)) or not(char_check(institute)) or not(char_check(spec_desc)) or not (char_check(notes)) or not(char_check(paperwork)) or not (char_check(digitized))):
			#brokenList.append([id,gift_num,acr,institute,date_recv,num_spec_rec,spec_desc,notes,paperwork,digitized])

			brokenList.append([self._ID,  tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8]])
			#else:
			self._ID += 1

					

			tempgift = in_gift(self._ID,  tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8])
			toReturn.append(tempList)	
			
			self._ID += 1

			print(brokenList)

			#line = file.readline()
			
		return toReturn

	
	def inLoans(self,file):
		tempList = []
		toReturn = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)
			temploans = in_loan(self._ID, request, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10], tempList[11], tempList[12], tempList[13], tempList[14], tempList[15], tempList[16], tempList[17], tempList[18], tempList[19], tempList[20], tempList[21], tempList[22])
			self._ID += 1
			
			toReturn.append(tempList)

			line = file.readline()
			
		return toReturn
		
	def inLoansRequest(self,file, request):
		tempList = []
		toReturn = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)
			temploans = in_loan(self._ID, request, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10], tempList[11], tempList[12], tempList[13], tempList[14], tempList[15], tempList[16], tempList[17], tempList[18], tempList[19], tempList[20], tempList[21], tempList[22])
			self._ID += 1
			
			toReturn.append(tempList)

			line = file.readline()
			
		return toReturn

	
	def outGifts(self,file):
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)

			tempgift = out_gift(self._ID, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10])
			self._ID += 1

			toReturn.append(tempList)

			line = file.readline()

		return toReturn
	
	def outLoans(self,file):
		tempList = []
		line = file.readline()
		toReturn = []
		while line != "":
			line = line.rstrip()
			tempList = comma_delimiter(line)

			tempList = quotes_remover(tempList)

			temploans = out_loan(self._ID, tempList[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6], tempList[7], tempList[8], tempList[9], tempList[10], tempList[11], tempList[12], tempList[13], tempList[14], tempList[15], tempList[16], tempList[17], tempList[18], tempList[19], tempList[20], tempList[21], tempList[22])
			self._ID += 1

			toReturn.append(tempList)

			line = file.readline()
		return toReturn

	## this function will save the stuff to a file with the specified output name
	def saveInventoryCatalogue(self, filename):
		file = open(filename, "w")
		file.write("Name|Continent|Population|PopulationDensity\n")
		i = 0
		for c in self._catalogue:
			if len(self._catalogue) > 0:
				toWrite = str(self._catalogue[c].getName() + "|" + self._catalogue[c].getContinent() + "|" +
				str(self._catalogue[c].getPopulation()) + "|" + "%.2f" %(self._catalogue[c].getPopDensity()) + "\n")
				file.write(toWrite)
				i += 1
			file.close()
		return i

## temp comment

## this function will check if a the string only contains characters to make sure it is a valid acroynum
def char_check(acro):
		# create a function that searchs through a string to see  if any digits are in the string
	return acro.isalpha()
		
	
## this function will check to make sure only a number value is present in this item
def num_check(num):
	try:
		temp = num.isdigit()
	except:
		temp = False
	return temp
		
## this function checks to see if this field has the correct number of items in it.
def item_num_check(items, num):
	return (items.split(" ") == num)
		
## this function returns to you the string which was selected to be the unknown string
def unknown_string():
	return unknown_str
		
		
	## a function to split a string by the comma, but only commas that have double quotes before and after the comma
def comma_delimiter(string):
	c = len(string)
	i = 1
	lastsplit = 0
	tokens = []
	while (i < c):
		if (i + 1 == c):
			tokens.append(string[lastsplit: c])
		elif ((string[i-1] == "\"") and (string[i] == ",") and (string [i+1] == "\"")):
			tokens.append(string[lastsplit+1:i-1])
			lastsplit = i+1
		i +=1
	return tokens

## a function to remove the quotes at the beginning and end of the string, as CSV files add quotes as well as commas to separate items
def quotes_remover(list):
	c = 0
	tempList = list
	while (c < len(tempList)):
		tempList[c].rstrip("\"")
		tempList[c].lstrip("\"")
		c +=1
	return tempList

"""
## follow this to turn all CSV files to have double quotes around the text

if __name__ == '__main__':
	
	temp = FileMang()
	temp.openFile('in_gifts_double_quotes.csv', 'in_gifts')
"""