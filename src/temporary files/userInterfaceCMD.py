"""from in_gifts import in_gift
from in_loans import in_loan
from out_gifts import out_gift
from out_loans import out_loan
#from filehandlerv2 import FileMang"""
import sys
import filehandlerv2

#print(file_manager)

class userInterfaceCMD:
    __instance = None
    __all_files_initialized = False
    __partial_files_initialized = False
    __initialized_files_list = []
    __non_initialized_files = []
    __files_list = []
    
    __file_manager = None
    
    @staticmethod
    def getInstance():
        if userInterfaceCMD.__instance == None:
            userInterfaceCMD
        return userInterfaceCMD.__instance
    
    
    def __init__(self):
        
        if (userInterfaceCMD.__instance !=None):
            raise Exception ("Singleton Design class")
        
        self.introduction()
        self.file_init_check()
        reply = self.options()
        ## the point of this is that we can have the user enter in a invalid choice, without filling up the stack with unnecessary function calls
        while(True):
            reply = self.options()
        print(reply)
    
    
    def introduction(self):
        print("Welcome to Python Herbarium Assistant\n"
        "This application as designed to help out " 
        "cateloguing the loans and gifts of different \nplants," 
        " kind of like a library book keeping system\n\n")
        
    
    def file_init_check(self):
        if (not userInterfaceCMD.__all_files_initialized):
            print("No files have been initialized")
            return
        
        print("The following files have not been inialized:")
        
        if (userInterfaceCMD.__inloansinit == 0):
            print("In Loans")
        if (userInterfaceCMD.__outloansinit == 0):
            print("Out Loans")
        if (userInterfaceCMD.__inloansrequestinit == 0):
            print("In Loans Request")
        if (userInterfaceCMD.__ingiftsinit == 0):
            print("In gifts")
        if (userInterfaceCMD.__outgiftsinit == 0):
            print("Out gifts")

    def options(self):
        print("\n\nPlease select from the following options: ")
        
        print("0) Setting up the Herbarium Assistant")
        print("1) Saving the current progress")
        print("2) Setting up the emailing system")
        print("3) Performing an operation on the catalogue")
        print("4) Exit")
        
        response = input("")
        
        if (response == "0"):
            self.import_files()
        elif(response == "1"):
            self.save_files()
        elif(response == "2"):
            self.email_setup()
        elif(response == "3"):
            self.catalogue_use()
        elif(response == "4"):
            self.exiting()
        else:
            print("Invalid input",response,"please try again.")
            return None
        return response

    def import_files(self):
        self.file_init_check()
        
        if (userInterfaceCMD.__file_manager == None):
            print("Setting up FileManager to aid in initialization of files")
            userInterfaceCMD.__FileMang = file_manager.file_manager()
    
    ## for now this function simply terminates the program, but what will happen is that we have to ensure that the files are saved, the integrity is checked and the nothing is "lost"
    def exiting(self):
        sys.exit()

    def email_setup(self):
        print("Not implemented Yet!")

    def catalogue_use(self):
        print("Not implemented Yet!")
        
        
    
    def save_files(self):
        print("Not implemented Yet!")

if __name__ == '__main__':
    interface = userInterfaceCMD()
    
    
    
    