# Herbarium Assistant Python

This is a verion of the Herbarium Assistant that is coded in python and stores the information locally rather than using mySQL
This software will implement the following:
	1) edit csv files to change the values currently stored in the csv files, create/remove columns, insert new data, remove old data
	2) allow the user to search through the catalogue, search in any field, search through a specific column, and within specific files
	3) also keep track of certain columns such as if all the plants have been returned or received
	4) will email the professor when a loan needs to be returned back to the department
	5) verify the information before it is inserted for certain columns such as plant names, institution names, dates, etc.

What is currently implemented:

	1) An interface through the command line to select specific options such as initializing the files, saving the changes, setting up the email to send the notifications to it, etc.
	2) the file manager or in this case the file fixer, which turns the csv file into an appropriate list of lists to be analyzed easily and stored in the catalogue structure
	3) the datatypes and having the ability to make custome data types to store in the csv
	4) the catalogue class which will have all the operations performed for cataloging which include the following:
		1) exiting
		2) searching
		3) inserting
		4) updating
		5) deleting
	5) in the catalogue container, created the appropriate method to call so that the saving function can easily be executed and run

What needs to be implemented:

	1) the ability to send emails as reminders
	2) the actual code that will be performed by each command such as searching, inserting, updating, deleting
	3) currently the code runs through cmd, not a gui, need to implement a gui next

### An important note
You can't run the program as the data is not avaliable to you. So if you would like to try it out, you will have to follow a similar structure to
how the .\src\workspace\DataRep\datatypes.txt is created for your data type, and you will have to ensure that your first row only contains the headers
that are needed by your type, and that every other row right after has the exact same number of columns, no more or no less.